const { payload } = require("./payload")
const { response } = require("./response")

module.exports = {
  payload,
  response
}
