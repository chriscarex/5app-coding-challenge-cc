module.exports = {
  root: true,
  parser: 'babel-eslint',
  extends: 'airbnb-base',
  plugins: ['import', 'prettier', 'babel', 'node'],
  env: {
    node: true,
    mocha: true
  },
  rules: {
    'import/extensions': [2, 'never'],
    'no-underscore-dangle': 0,
    'lines-between-class-members': 0,
    'arrow-body-style': 0,
    'no-return-assign': 0,
    'no-param-reassign': 0,
    'no-plusplus': 0,
    'comma-dangle': 0,
    'func-names': [2, 'as-needed'],
    'import/prefer-default-export': 0,
    'arrow-parens': 0,
    semi: [2, 'never'],
    'max-len': [2, 200, 4, { ignoreUrls: true }],
    'no-unexpected-multiline': 2
  }
}
