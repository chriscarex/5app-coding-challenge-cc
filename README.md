# 5app-coding-challenge-cc

NodeJS webservice which modifies an incoming JSON `POST` payload and returns a response.

The request body should be a JSON object with property `payload` with an Array of items, e.g.:

```
{
  "payload": [
    {
      "name": "Molly",
      "count": 12,
      "logos": [{
        "size": "16x16",
        "url": "https://example.com/16x16.png"
      },
      ...]
    },
    ...
  ]
```

The response should be a JSON object with property `response` with an Array of items, e.g.:

```
{
	"response": [
		{
			"name": "Molly",
			"count": 12,
			"thumbnail": "https://example.com/64x64.png"
		},
		{
			"name": "Polly",
			"count": 4,
			"thumbnail": "https://example.com/64x64.png"
		}
	]
}
```

On the response:

- `name` and `count` match their respective properties from the payload
- The payload is filtered, so only items with a `count` greater than `1` are returned.
- The `thumbnail` is a `url` selected from the payload item's list of `logos` no larger than 128x128 but no smaller than 64x64.

## Installation

```
  git clone https://gitlab.com/chriscarex/5app-coding-challenge-cc.git
  npm i
  cp .envDemoFile .env
  [modify environment and port number as you like in the .env file]
  npm run dev  (for local development)

```

## Local testing

```
  npm run test  (runs unit and integration tests)

  or try:

  curl -d '{"payload": [{"name": "Molly","count": 12,"logos": [{"size": "16x16","url": "https%3A%2F%2Fexample.com%2F16x16.png"},{"size": "64x64","url": "https%3A%2F%2Fexample.com%2F64x64.png"}]}]}' -H "Content-Type: application/json" -H "Accept: application/json" -X POST http://localhost:3000/modify

```

# Submit

To submit your service please enter the details in https://fiveapp-coding-challenge.herokuapp.com
