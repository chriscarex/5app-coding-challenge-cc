/* eslint no-console: 0 */
const express = require('express')
const dotenv = require('dotenv')
const index = require('./routes/index')

if (process.env.NODE_ENV !== 'production') {
  dotenv.config()
}

const app = express()

app.use((req, res, next) => {
  res.setHeader('Referrer-Policy', 'strict-origin-when-cross-origin')
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, authorization, x-auth-token')
  res.setHeader('Access-Control-Expose-Headers', 'x-auth-token, authorization')
  res.removeHeader('X-Powered-By')

  next()
})

// app.use(bodyParser.json({ limit: "1mb", extended: true }))
app.use(express.json())

// handle promise rejections
process.on('unhandledRejection', reason => {
  console.log('Unhandled Rejection at:', reason.stack || reason)
})

app.use('/', index)

const port = process.env.PORT || '3000'
app.set('port', port)

app.listen(port, () => console.log(`App listening on port ${process.env.PORT}!`))

module.exports = app
