const { addCorsHeaders, payloadFilter } = require('../utils')

module.exports = {
  modifyPayload(req, res) {
    addCorsHeaders(res)

    const { payload } = req.body

    if (payload) {
      const response = payloadFilter(payload)

      return res.json({ response })
    }

    return res.json({ response: [] })
  }
}
