process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../server')

/* eslint-disable-next-line */
const should = chai.should()
const { payload, response } = require('../../../samples')

chai.use(chaiHttp)

describe('/modify', () => {
  describe('/POST', () => {
    it('it should return the sample response', done => {
      chai
        .request(server)
        .post('/modify')
        .send({ payload })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.be.deep.equal({ response })
          done()
        })
    })
    it('it should return an empty response', done => {
      chai
        .request(server)
        .post('/modify')
        .send({ payload: [] })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.be.deep.equal({ response: [] })
          done()
        })
    })
  })
})
