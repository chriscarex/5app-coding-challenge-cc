const express = require('express')
const { modifyPayload } = require('./modifyPayload')

const router = express.Router()

router.route('/modify').post(modifyPayload)

module.exports = router
