const { checkCount } = require('./checkCount')
const { selectThumbnail } = require('./selectThumbnail')

module.exports = {
  payloadFilter(payload) {
    const response = []
    if (payload.length > 0) {
      const validCountItems = payload.filter(item => checkCount(item))

      if (validCountItems.length > 0) {
        validCountItems.map(item => {
          if (item.name) {
            const thumbnail = selectThumbnail(item.logos)

            if (thumbnail !== '') {
              response.push({
                name: item.name,
                count: item.count,
                thumbnail
              })
            }
          }

          return null
        })
      }

      return response
    }

    return response
  }
}
