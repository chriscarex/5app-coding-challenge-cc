const { addCorsHeaders } = require('./addCorsHeaders')
const { payloadFilter } = require('./payloadFilter')
const { checkCount } = require('./checkCount')
const { checkSize } = require('./checkSize')
const { selectThumbnail } = require('./selectThumbnail')

module.exports = {
  addCorsHeaders,
  payloadFilter,
  checkCount,
  selectThumbnail,
  checkSize
}
