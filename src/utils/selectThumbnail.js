const { checkSize } = require('./checkSize')

module.exports = {
  selectThumbnail(logos) {
    let thumbnail = ''

    if (logos) {
      if (logos.length > 0) {
        logos.map(logo => {
          if (thumbnail === '') {
            if (logo.url) {
              if (logo.size) {
                const isSizeOk = checkSize(logo.size)

                if (isSizeOk) {
                  thumbnail = logo.url
                }
              }
            }
          }

          return null
        })
      }
    }

    return thumbnail
  }
}
