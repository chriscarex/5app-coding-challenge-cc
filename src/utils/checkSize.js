module.exports = {
  checkSize(size) {
    const dimensions = size.split('x')

    if (dimensions.length === 2) {
      const x = parseInt(dimensions[0], 10)
      const y = parseInt(dimensions[1], 10)
      return x > 63 && y > 63 && x < 129 && y < 129
    }

    return false
  }
}
