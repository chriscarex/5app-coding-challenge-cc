const { expect } = require('chai')
const { checkSize } = require('..')

describe('checkSize', () => {
  it('returns true if x and y above 63 and below 129', () => {
    const size = '64x64'
    expect(checkSize(size)).to.equal(true)
  })
  it('returns false if x below 64', () => {
    const size = '63x64'
    expect(checkSize(size)).to.equal(false)
  })
  it('returns false if y below 64', () => {
    const size = '64x63'
    expect(checkSize(size)).to.equal(false)
  })
  it('returns false if x above 128', () => {
    const size = '129x64'
    expect(checkSize(size)).to.equal(false)
  })
  it('returns false if y above 128', () => {
    const size = '64x129'
    expect(checkSize(size)).to.equal(false)
  })
  it('returns false if only x provided', () => {
    const size = '64'
    expect(checkSize(size)).to.equal(false)
  })
})
