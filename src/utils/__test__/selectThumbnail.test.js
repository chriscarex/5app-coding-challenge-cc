const { expect } = require('chai')
const { selectThumbnail } = require('..')

describe('selectThumbnail', () => {
  it('returns the url of the biggest thumbnail', () => {
    const item1 = {
      logos: [
        {
          size: '16x16',
          url: 'https://example.com/16x16.png'
        },
        {
          size: '64x64',
          url: 'https://example.com/64x64.png'
        }
      ]
    }
    expect(selectThumbnail(item1.logos)).to.deep.equal(item1.logos[1].url)
  })

  it('returns the url of the smallest thumbnail', () => {
    const item2 = {
      logos: [
        {
          size: '129x129',
          url: 'https://example.com/129x129.png'
        },
        {
          size: '64x64',
          url: 'https://example.com/64x64.png'
        }
      ]
    }
    expect(selectThumbnail(item2.logos)).to.deep.equal(item2.logos[1].url)
  })

  it('returns false if count is undefined', () => {
    const item3 = { logos: [] }
    expect(selectThumbnail(item3.logos)).to.deep.equal('')
  })
})
