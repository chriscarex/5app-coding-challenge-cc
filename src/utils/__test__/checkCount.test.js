const { expect } = require('chai')
const { checkCount } = require('..')

describe('checkCount', () => {
  it('returns false if count === 0', () => {
    const item = { count: 0 }
    expect(checkCount(item)).to.equal(false)
  })

  it("returns false if count === 'string'", () => {
    const item = { count: 'abc' }
    expect(checkCount(item)).to.equal(false)
  })

  it('returns false if count is undefined', () => {
    const item = {}
    expect(checkCount(item)).to.equal(false)
  })

  it('returns false if count is > 0', () => {
    const item = { count: 10 }
    expect(checkCount(item)).to.equal(true)
  })

  it("returns false if count is > 0 and 'string'", () => {
    const item = { count: '10' }
    expect(checkCount(item)).to.equal(true)
  })
})
