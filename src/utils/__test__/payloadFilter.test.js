const { expect } = require('chai')
const { payloadFilter } = require('../payloadFilter')
const { payload, response } = require('../../../samples')

describe('payloadFilter', () => {
  it('returns correct response', () => {
    expect(payloadFilter(payload)).to.deep.equal(response)
  })
})
