module.exports = {
  checkCount(item) {
    return parseInt(item.count, 10) > 0
  }
}
